import static spark.Spark.*;
import spark.servlet.SparkApplication;

//public class JettyApp implements SparkApplication {
//	@Override
//	public void init() {
//		get("/", (req, res) -> "hello jetty application");
//	}
//}


public class JettyApp {
	public static void main(String[] args) {
		//get("/", (req, res) -> "<h1>It works. Bully for you.</h1>");
		//get("/",this::sayHello);
		
		Router router = new Router();
		router.init();
	}
	
	
}